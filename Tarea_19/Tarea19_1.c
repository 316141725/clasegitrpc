#include <stdio.h>
int main(int argc, char const *argv[])
{
  char arreglo[6];
  arreglo[0]='A';
  arreglo[1]='B';
  arreglo[2]='C';
  arreglo[3]='D';
  arreglo[4]='E';
  arreglo[5]='F';
  // EXTRAEMOS LOS VALORES INDICANDO SU SUBINDICE
  printf("El subindice 0 es: %c\n",arreglo[0]);
  printf("El subindice 1 es: %c\n",arreglo[1]);
  printf("El subindice 2 es: %c\n",arreglo[2]);
  printf("El subindice 3 es: %c\n",arreglo[3]);
  printf("El subindice 4 es: %c\n",arreglo[4]);
  printf("El subindice 5 es: %c\n",arreglo[5]);
  return 0;
}

